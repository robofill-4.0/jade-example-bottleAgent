Minimal BottleAgent example
===================

Usage
---------

Start the `run.cmd`

Explanation
-----------------

* the source files are compiled
* jade.Boot -gui is instanciated

Notes
---------
Manual usage:
```shell
// Compile (optional)
> javac -d classes src/*.java
// Start Main Container
> java -cp lib/jade.jar jade.Boot -gui
```